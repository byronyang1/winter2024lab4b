public class Panda {
	
	private String type;
	private String name;
	private int age;
	
	public void setAge(int age) {
		this.age = age;
	}
	
	public String getType() {
		return this.type;
	}
	
	public String getName() {
		return this.name;
	}
	
	public int getAge() {
		return this.age;
	}
	
	public void doFunActivity(){
		if(this.type.equals("Giant Panda")){
			System.out.println("Fun Activity: Rolling");
		} else {
			System.out.println("Fun activity: Swinging");
		}
	}
	
	public void isAMasterOfKungFu(){
		if(this.name.equals("Po") || this.name.equals("Shifu") ){
			System.out.println(name + " is a master of Kung Fu.");
		} else {
			System.out.println(name + " is not a master of Kung Fu.");
		}
	}
	
	public Panda(String type, String name, int age) {
		this.type = type;
		this.name = name;
		this.age = age;
	}
}