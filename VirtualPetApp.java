import java.util.Scanner;

public class VirtualPetApp{
	public static void main (String [] args){
		Scanner reader = new Scanner(System.in);
		Panda embarrassment[] = new Panda[1];
		for(int i = 0; i < embarrassment.length; i++){
			System.out.println("What is the type of panda #" + (i+1) + " of the embarrassment?");
			String type = reader.nextLine();
			System.out.println("Give the name of a panda from the movie 'Kung Fu Panda'?");
			String name = reader.nextLine();
			System.out.println("What is the age of the panda");
			int age = reader.nextInt();
			embarrassment[i] = new Panda(type, name, age);
		}
		System.out.println(embarrassment[embarrassment.length-1].getType());
		System.out.println(embarrassment[embarrassment.length-1].getName());
		System.out.println(embarrassment[embarrassment.length-1].getAge());
        System.out.println("What is the new age of the last animal in the array?");
        int age = reader.nextInt();
		embarrassment[embarrassment.length-1].setAge(age);
		System.out.println(embarrassment[embarrassment.length-1].getType());
		System.out.println(embarrassment[embarrassment.length-1].getName());
		System.out.println(embarrassment[embarrassment.length-1].getAge());			
	}
}